package ru.malykhin.tm.service;

import ru.malykhin.tm.api.ConnectionProvider;
import ru.malykhin.tm.entity.Project;
import ru.malykhin.tm.entity.Task;
import ru.malykhin.tm.repository.ProjectRepository;
import ru.malykhin.tm.repository.TaskRepository;

import java.util.List;

public class ProjectService {

    private final ConnectionProvider provider;

    private ProjectRepository getRepo() {
        return new ProjectRepository(provider.getConnection());
    }

    public ProjectService(ConnectionProvider provider) {
        this.provider = provider;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return getRepo().create(name);
    }

    public void load(Project project) {
        getRepo().load(project);
    }

    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return getRepo().create(name, description);
    }

    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return getRepo().update(id, name, description);
    }

    public void clear() {
        getRepo().clear();
    }

    public Project findByIndex(int index) {
        return getRepo().findByIndex(index);
    }

    public Project findByName(String name) {
        return getRepo().findByName(name);
    }

    public Project findById(Long id) {
        return getRepo().findById(id);
    }

    public Project removeByIndex(int index) {
        return getRepo().removeByIndex(index);
    }

    public Project removeById(Long id) {
        return getRepo().removeById(id);
    }

    public Project removeByName(String name) {
        return getRepo().removeByName(name);
    }

    public List<Project> findAll() {
        return getRepo().findAll();
    }


}
