package ru.malykhin.tm.service;

import ru.malykhin.tm.api.ConnectionProvider;
import ru.malykhin.tm.entity.Task;
import ru.malykhin.tm.repository.TaskRepository;

import java.sql.SQLException;
import java.util.List;

public class TaskService {



    private final ConnectionProvider provider;

    private TaskRepository getRepo () {
        return new TaskRepository(provider.getConnection());
    }


    public TaskService(ConnectionProvider provider) {
        this.provider = provider;
    }

    public Task create(String name) throws SQLException {
        return getRepo().create(name);
    }

    public Task findByIndex(int index) {
        return null;
    }

    public Task findByName(String name) throws SQLException {
        return getRepo().findByName(name);
    }

    public void load(Task task) {

    }

    public void removeById(Long id) throws SQLException {
        getRepo().removeById(id);
    }

    public void removeByName(String name) throws SQLException {
        getRepo().removeByName(name);
    }

    public Task findById(Long id) throws SQLException {
        return getRepo().findById(id);
    }

    public void saveAll() {

    }

    public void loadAll() {

    }

    public void clear() {
        getRepo().clear();
    }

    public List<Task> findAll() throws SQLException {
        return getRepo().findAll();
    }

}
