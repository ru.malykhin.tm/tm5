package ru.malykhin.tm.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService {
    private static final String FILE = "/application.properties";
    private static final InputStream stream  = PropertyService.class.getResourceAsStream(FILE);
    private static final Properties properties = new Properties();

    static {
        try {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPort() {
        return properties.getProperty("jdbc.port");
    }

    public String getHost() {
        return properties.getProperty("jdbc.host");
    }

    public String getLogin() {
        return properties.getProperty("jdbc.login");
    }

    public String getPassword() {
        return properties.getProperty("jdbc.password");
    }

    public String getDB() {
        return properties.getProperty("jdbc.database");
    }

    public String getDBUrl() {
        return "jdbc:mysql://"+ getHost() +":" + getPort()+"/"+getDB()+"?serverTimezone=Europe/Moscow";

    }
}

