package ru.malykhin.tm;

import ru.malykhin.tm.api.ConnectionProvider;
import ru.malykhin.tm.controller.ProjectController;
import ru.malykhin.tm.controller.SystemController;
import ru.malykhin.tm.controller.TaskController;
import ru.malykhin.tm.endpoint.ProjectEndpoint;
import ru.malykhin.tm.endpoint.TaskEndpoint;
import ru.malykhin.tm.repository.ProjectRepository;
import ru.malykhin.tm.repository.TaskRepository;
import ru.malykhin.tm.service.ProjectService;
import ru.malykhin.tm.service.TaskService;
import ru.malykhin.tm.service.PropertyService;

import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

import static ru.malykhin.tm.constant.TerminalConst.*;

/**
 * Тестовое приложение
 */
public class App {




    private final PropertyService propertyService = new PropertyService();

    private final ConnectionProvider provider = new ConnectionProvider() {
        @Override
        public Connection getConnection() {
            try {
                final String jdbcURL = propertyService.getDBUrl();

                return DriverManager.getConnection(jdbcURL,
                        propertyService.getLogin(),
                        propertyService.getPassword());
            } catch (Exception throwables) {
                throwables.printStackTrace();
                return null;
            }
        }
    };

    private final ProjectService projectService = new ProjectService(provider);

    private final ProjectController projectController = new ProjectController(projectService);

    private final TaskService taskService = new TaskService(provider);

    private final TaskController taskController = new TaskController(taskService);

    private final SystemController systemController = new SystemController();

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService);

    public static void main(final String[] args) throws SQLException {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        Endpoint.publish("http://0.0.0.0/TaskEndpoint?wsdl",app.taskEndpoint);
        Endpoint.publish("http://0.0.0.0/ProjectEndpoint?wsdl",app.projectEndpoint);
        app.systemController.displayWelcome();
    }

    public void run(final String[] args) throws SQLException {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    public int run(final String param) throws SQLException {
        if (param == null || param.isEmpty()) return -1;
        switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return  systemController.displayAbout();
            case HELP: return  systemController.displayHelp();
            case SAVE: return  systemController.saveAll(projectController,taskController);
            case LOAD: return  systemController.loadAll(projectController,taskController);
            case EXIT: return  systemController.displayExit();


            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_SAVE: return projectController.saveProject();
            case PROJECT_LOAD: return projectController.loadProject();
            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_VIEW: return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();

            case TASK_LIST: return taskController.listTask();
            case TASK_SAVE: return taskController.saveTask();
            case TASK_LOAD: return taskController.loadTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_CREATE: return taskController.createTask();
            case TASK_VIEW: return taskController.viewTaskByIndex();

            default: return systemController.displayError();
        }
    }

}
