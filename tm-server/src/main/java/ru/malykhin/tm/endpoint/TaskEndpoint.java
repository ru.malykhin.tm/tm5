package ru.malykhin.tm.endpoint;

import ru.malykhin.tm.entity.Task;
import ru.malykhin.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService
public class TaskEndpoint {

    private TaskService taskService;

    public TaskEndpoint() {

    }

    public TaskEndpoint(TaskService taskService) {
        this.taskService = taskService;
    }

    @WebMethod
    public List<Task> findAll() throws SQLException {
        return taskService.findAll();
    }

    @WebMethod
    public Task createTask(String name) throws SQLException {
        return taskService.create(name);
    }

    @WebMethod
    public Task findByName(String name) throws SQLException {
        return taskService.findByName(name);
    }
    @WebMethod
    public void removeById(Long id) throws SQLException {
        taskService.removeById(id);
    }
    @WebMethod
    public void removeByName(String name) throws SQLException {
        taskService.removeByName(name);
    }
    @WebMethod
    public Task findById(Long id) throws SQLException {
        return taskService.findById(id);
    }
}
