package ru.malykhin.tm.repository;

import ru.malykhin.tm.controller.TaskController;
import ru.malykhin.tm.entity.Task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository {

    private List<Task> tasks = new ArrayList<>();

    public TaskRepository(Connection connection) {
        super(connection);
    }

    public Task create(String name) throws SQLException {
        Task task = new Task(name);
        String query = "insert into `tasks` (`id`,`name`,`description`) VALUES (?, ?, ?)";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setLong(1,task.getId());
        st.setString(2,task.getName());
        st.setString(3,task.getDescription());
        st.execute();
        return task;
    }

    public Task findByIndex(final int index) {
        return null;
    }

    public Task findByName(final String name) throws SQLException {
        String query = "select * from  `tasks` where `name` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setString(1,name);
        final ResultSet resultSet = st.executeQuery();
        Boolean hN = resultSet.next();
        if (!hN) return null;
        return fetch(resultSet);
    }

    public void removeById(final Long id) throws SQLException {
        String query = "delete from  `tasks` where `id` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setLong(1,id);
        st.execute();
    }

    public void removeByName(final String name) throws SQLException {
        String query = "delete from  `tasks` where `name` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setString(1,name);
        st.execute();
    }

    public Task findById(final Long id) throws SQLException {
        String query = "select * from  `tasks` where `id` = ?";
        PreparedStatement st = getConnection().prepareStatement(query);
        st.setLong(1,id);
        final ResultSet resultSet = st.executeQuery();
        Boolean hN = resultSet.next();
        if (!hN) return null;
        return fetch(resultSet);
    }

    private Task fetch(final ResultSet row) throws SQLException {
        if (row == null) return null;
        Task task = new Task();
        task.setId(row.getLong("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        return task;
    }



    public void clear() {

    }

    public List<Task> findAll() throws SQLException {
        String query = "select * from  `tasks` ";
        PreparedStatement st = getConnection().prepareStatement(query);
        final ResultSet resultSet = st.executeQuery();
        List<Task> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(fetch(resultSet));
        }
        st.close();
        return result;
    }

}
