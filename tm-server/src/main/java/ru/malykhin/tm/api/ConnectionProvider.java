package ru.malykhin.tm.api;

import java.sql.Connection;

public interface ConnectionProvider {
    Connection getConnection();
}
