package ru.malykhin.tm;


import ru.malykhin.tm.endpoint.TaskEndpoint;
import ru.malykhin.tm.endpoint.TaskEndpointService;

public class Client {
    public static void main(String[] args) {
        TaskEndpointService taskEndpointService = new TaskEndpointService();
        TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
        System.out.println(taskEndpoint.findAll());
    }


}
