
package ru.malykhin.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.malykhin.tm.endpoint package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Task_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "Task");
    private final static QName _CreateTask_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "createTask");
    private final static QName _CreateTaskResponse_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "createTaskResponse");
    private final static QName _FindAll_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "findAll");
    private final static QName _FindAllResponse_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "findAllResponse");
    private final static QName _FindByIndex_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "findByIndex");
    private final static QName _FindByIndexResponse_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "findByIndexResponse");
    private final static QName _LoadAll_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "loadAll");
    private final static QName _LoadAllResponse_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "loadAllResponse");
    private final static QName _SaveAll_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "saveAll");
    private final static QName _SaveAllResponse_QNAME = new QName("http://endpoint.tm.malykhin.ru/", "saveAllResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.malykhin.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link CreateTask }
     * 
     */
    public CreateTask createCreateTask() {
        return new CreateTask();
    }

    /**
     * Create an instance of {@link CreateTaskResponse }
     * 
     */
    public CreateTaskResponse createCreateTaskResponse() {
        return new CreateTaskResponse();
    }

    /**
     * Create an instance of {@link FindAll }
     * 
     */
    public FindAll createFindAll() {
        return new FindAll();
    }

    /**
     * Create an instance of {@link FindAllResponse }
     * 
     */
    public FindAllResponse createFindAllResponse() {
        return new FindAllResponse();
    }

    /**
     * Create an instance of {@link FindByIndex }
     * 
     */
    public FindByIndex createFindByIndex() {
        return new FindByIndex();
    }

    /**
     * Create an instance of {@link FindByIndexResponse }
     * 
     */
    public FindByIndexResponse createFindByIndexResponse() {
        return new FindByIndexResponse();
    }

    /**
     * Create an instance of {@link LoadAll }
     * 
     */
    public LoadAll createLoadAll() {
        return new LoadAll();
    }

    /**
     * Create an instance of {@link LoadAllResponse }
     * 
     */
    public LoadAllResponse createLoadAllResponse() {
        return new LoadAllResponse();
    }

    /**
     * Create an instance of {@link SaveAll }
     * 
     */
    public SaveAll createSaveAll() {
        return new SaveAll();
    }

    /**
     * Create an instance of {@link SaveAllResponse }
     * 
     */
    public SaveAllResponse createSaveAllResponse() {
        return new SaveAllResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Task }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Task }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "Task")
    public JAXBElement<Task> createTask(Task value) {
        return new JAXBElement<Task>(_Task_QNAME, Task.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "createTask")
    public JAXBElement<CreateTask> createCreateTask(CreateTask value) {
        return new JAXBElement<CreateTask>(_CreateTask_QNAME, CreateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "createTaskResponse")
    public JAXBElement<CreateTaskResponse> createCreateTaskResponse(CreateTaskResponse value) {
        return new JAXBElement<CreateTaskResponse>(_CreateTaskResponse_QNAME, CreateTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "findAll")
    public JAXBElement<FindAll> createFindAll(FindAll value) {
        return new JAXBElement<FindAll>(_FindAll_QNAME, FindAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "findAllResponse")
    public JAXBElement<FindAllResponse> createFindAllResponse(FindAllResponse value) {
        return new JAXBElement<FindAllResponse>(_FindAllResponse_QNAME, FindAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIndex }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByIndex }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "findByIndex")
    public JAXBElement<FindByIndex> createFindByIndex(FindByIndex value) {
        return new JAXBElement<FindByIndex>(_FindByIndex_QNAME, FindByIndex.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByIndexResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindByIndexResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "findByIndexResponse")
    public JAXBElement<FindByIndexResponse> createFindByIndexResponse(FindByIndexResponse value) {
        return new JAXBElement<FindByIndexResponse>(_FindByIndexResponse_QNAME, FindByIndexResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "loadAll")
    public JAXBElement<LoadAll> createLoadAll(LoadAll value) {
        return new JAXBElement<LoadAll>(_LoadAll_QNAME, LoadAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "loadAllResponse")
    public JAXBElement<LoadAllResponse> createLoadAllResponse(LoadAllResponse value) {
        return new JAXBElement<LoadAllResponse>(_LoadAllResponse_QNAME, LoadAllResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveAll }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveAll }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "saveAll")
    public JAXBElement<SaveAll> createSaveAll(SaveAll value) {
        return new JAXBElement<SaveAll>(_SaveAll_QNAME, SaveAll.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveAllResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveAllResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.malykhin.ru/", name = "saveAllResponse")
    public JAXBElement<SaveAllResponse> createSaveAllResponse(SaveAllResponse value) {
        return new JAXBElement<SaveAllResponse>(_SaveAllResponse_QNAME, SaveAllResponse.class, null, value);
    }

}
